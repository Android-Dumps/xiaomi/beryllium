#
# Copyright (C) 2021 The Android Open Source Project
# Copyright (C) 2021 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

add_lunch_combo omni_beryllium-user
add_lunch_combo omni_beryllium-userdebug
add_lunch_combo omni_beryllium-eng
